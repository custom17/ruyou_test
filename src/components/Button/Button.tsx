//core
import React, { FC } from "react";
import cn from "classnames";
//styles
import styles from "@components/Button/Button.module.scss";
//interfaces
interface IButtonProps {
  title: string;
  onClick: any;
  disabled?: boolean;
  loading?: boolean;
}

const Button: FC<IButtonProps> = ({ title, disabled, loading, onClick }) => {
  return (
    <button
      disabled={disabled}
      className={cn(styles.root, { [styles.disabled]: disabled })}
      onClick={onClick}
    >
      {loading ? "Ждите ..." : title}
    </button>
  );
};

export default Button;
