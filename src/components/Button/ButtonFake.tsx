//core
import React, { FC } from "react";
//styles
import styles from "@components/Button/Button.module.scss";
//interfaces
interface IButtonFakeProps {
  title: string;
}

const ButtonFake: FC<IButtonFakeProps> = ({ title }) => {
  return <div className={styles.root}>{title}</div>;
};

export default ButtonFake;
