//core
import React, { ChangeEvent, FC } from "react";
//components
import { Label } from "@components/Label";
//styles
import styles from "@components/Input/Input.module.scss";

//interfaces
interface IInputProps {
  autoFocus?: boolean;
  value: string | undefined;
  label: string;
  onChange: any;
  placeholder?: string;
  disabled?: boolean;
}

const Input: FC<IInputProps> = ({
  autoFocus,
  value,
  label,
  onChange,
  placeholder,
  disabled,
}) => {
  return (
    <div className={styles.root}>
      <Label label={label} />
      <input
        autoFocus={autoFocus}
        value={value}
        disabled={disabled}
        className={styles.input}
        placeholder={placeholder}
        type={"text"}
        onChange={(val: ChangeEvent<HTMLInputElement>) =>
          onChange(val.target.value)
        }
      />
    </div>
  );
};

export default Input;
