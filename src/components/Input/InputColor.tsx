//core
import React, { ChangeEvent, FC } from "react";
//styles
import styles from "@components/Input/Input.module.scss";
//interfaces
interface IInputColorProps {
  value?: string;
  id?: string;
  children: any;
  onChange: any;
}

//constants
const DEFAULT_COLOR = "#FFA500";
const DEFAULT_ID = "select_color";

const InputColor: FC<IInputColorProps> = ({
  value = DEFAULT_COLOR,
  id = DEFAULT_ID,
  children,
  onChange,
}) => {
  return (
    <label htmlFor={id} className={styles.inputColor}>
      <input
        id={id}
        type={"color"}
        value={value}
        className={styles.previewColor}
        onChange={(val: ChangeEvent<HTMLInputElement>) => {
          onChange(val.target.value);
        }}
      />
      {children}
    </label>
  );
};

export default InputColor;
