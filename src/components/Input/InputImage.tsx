//core
import React, { ChangeEvent, FC, useCallback } from "react";
import cn from "classnames";
//data
import previewPic from "@assets/images/preview.svg";
//components
import { Label } from "@components/Label";
//styles
import styles from "@components/Input/Input.module.scss";

//interfaces
interface IInputImageProps {
  value?: string | undefined;
  label: string;
  onChange: any;
  disabled?: boolean;
  previewImage?: File;
}

const InputImage: FC<IInputImageProps> = ({
  label,
  onChange,
  previewImage,
  disabled,
}) => {
  const handleOnChange = (file: File) => {
    onChange(file);
  };

  const dragOver = useCallback((event: ChangeEvent<any>) => {
    event.preventDefault();
  }, []);

  const drop = useCallback((event: any) => {
    event.preventDefault();
    if (event.dataTransfer.items) {
      [...event.dataTransfer.items].forEach((item) => {
        if (item.kind === "file") {
          const file = item.getAsFile();
          if (file.type.includes("image/")) {
            handleOnChange(file);
          }
        }
      });
    } else {
      [...event.dataTransfer.files].forEach((file) => {
        if (file.type.includes("image/")) {
          handleOnChange(file);
        }
      });
    }
  }, []);

  return (
    <div className={styles.root} onDrop={drop} onDragOver={dragOver}>
      <Label label={label} />
      <input
        disabled={disabled}
        id="preview-image"
        type="file"
        accept="image/*"
        className={styles.inputImage}
        onChange={(event: ChangeEvent<any>) =>
          handleOnChange(event.target.files[0])
        }
      />
      <label htmlFor="preview-image">
        <div className={styles.preview}>
          <img
            src={previewImage ? URL.createObjectURL(previewImage) : previewPic}
            alt="preview"
            className={cn({ [styles.previewImage]: previewImage })}
          />
        </div>
      </label>
    </div>
  );
};

export default InputImage;
