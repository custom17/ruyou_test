//core
import React, { FC } from "react";
//styles
import styles from "@components/Label/Label.module.scss";

//interfaces
interface ILabelProps {
  label: string;
}

const Label: FC<ILabelProps> = ({ label }) => {
  return <label className={styles.root}>{label}</label>;
};

export default Label;
