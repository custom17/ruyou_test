//core
import React from "react";
import { Link, useLocation } from "react-router-dom";
import cn from "classnames";
//data
import { book } from "@routes/book";
//styles
import styles from "@components/Navigation/Navigation.module.scss";

const Navigation = () => {
  const location = useLocation();

  return (
    <div className={styles.root}>
      {book.map((page) => {
        return (
          <Link
            key={page.to}
            to={page.to}
            className={cn(styles.title, {
              [styles.select]: page.to === location.pathname,
            })}
          >
            {page.title}
          </Link>
        );
      })}
    </div>
  );
};

export default Navigation;
