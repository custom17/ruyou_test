//core
import React, { FC } from "react";
//data
import cross from "@assets/images/cross.svg";
//components
import { InputColor } from "@components/Input";
//styles
import styles from "@components/Scuare/Square.module.scss";

//interfaces
interface ISquareProps {
  id?: string | number;
  color: string;
  remove?: any;
  update?: any;
}

const Square: FC<ISquareProps> = ({ id, color, remove, update }) => {
  return (
    <div className={styles.root}>
      {update ? (
        <InputColor
          id={`${color}_${id}`}
          value={color}
          onChange={(color: string) => update(color)}
        >
          <div className={styles.content} style={{ backgroundColor: color }} />
        </InputColor>
      ) : (
        <div className={styles.content} style={{ backgroundColor: color }} />
      )}
      {remove && (
        <div className={styles.cross} onClick={remove}>
          <img src={cross} alt="remove" />
        </div>
      )}
    </div>
  );
};

export default Square;
