//core
import React from "react";
import { Provider } from "react-redux";
import { createRoot } from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
//redux
import store from "@redux/store";
//components
import App from "@routes/App";
//styles
import "./index.scss";

const rootElement = document.getElementById("app");

if (!rootElement) throw new Error("Failed to find the root element");

createRoot(rootElement).render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);
