import { ReactElement } from "react";

export interface IPage {
  id: number;
  to: string;
  title: string;
  element: ReactElement;
}
