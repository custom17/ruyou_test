export interface IUser {
  name: string;
  surname: string;
  patronymic: string;
  photo: File | undefined;
}
