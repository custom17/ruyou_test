//core
import React from "react";
import { Outlet } from "react-router-dom";
//components
import { Navigation } from "@components/Navigation";
//styles
import styles from "@layouts/Layout/Layout.module.scss";

const Layout = () => {
  return (
    <div className={styles.root}>
      <div className={styles.content}>
        <Navigation />
        <Outlet />
      </div>
    </div>
  );
};

export default Layout;
