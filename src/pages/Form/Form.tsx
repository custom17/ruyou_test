//core
import React, { useCallback, useMemo, useRef, useState } from "react";
//components
import { Input, InputImage } from "@components/Input";
import { Label } from "@components/Label";
import { Button } from "@components/Button";
//styles
import styles from "@pages/Form/Form.module.scss";
//interfaces
import { IUser } from "@interfaces/IUser";

//constants
const URL = "https://test-job.pixli.app/send.php";
const USER: IUser = {
  name: "",
  surname: "",
  patronymic: "",
  photo: undefined,
};

const Form = () => {
  const responseRef = useRef<HTMLInputElement>(null);
  const [user, setUser] = useState<IUser>(USER);
  const [loading, setLoading] = useState<boolean>(false);

  const changeUser = useCallback((field: string, value: string | File) => {
    setUser((prev: IUser) => {
      return {
        ...prev,
        [field]: value,
      };
    });
  }, []);

  const disabledButton = useMemo(() => {
    let disabled = false;
    for (const key in user) {
      if (!(user as any)[key]) {
        disabled = true;
        break;
      }
    }
    return disabled;
  }, [user]);

  const save = async () => {
    try {
      setLoading(true);

      let data = new FormData();
      data.set("action", "send_data");
      data.set("id", "1");
      data.set("contact[name]", user.name);
      data.set("contact[surname]", user.surname);
      data.set("contact[patronymic]", user.patronymic);
      data.set("image", user.photo || "");

      let response = await fetch(URL, {
        method: "POST",
        body: data,
      });
      response = await response.json();

      responseRef.current &&
        (responseRef.current.innerHTML = JSON.stringify(response));

      setUser(USER);

      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  return (
    <div className={styles.root}>
      <div>
        <Input
          disabled={loading}
          autoFocus
          label={"Имя"}
          value={user.name}
          onChange={(val: string) => changeUser("name", val)}
        />
        <Input
          disabled={loading}
          label={"Фамилия"}
          value={user.surname}
          onChange={(val: string) => changeUser("surname", val)}
        />
        <Input
          disabled={loading}
          label={"Отчество"}
          value={user.patronymic}
          onChange={(val: string) => changeUser("patronymic", val)}
        />
        <InputImage
          disabled={loading}
          label={"Фото"}
          onChange={(val: File) => changeUser("photo", val)}
          previewImage={user.photo}
        />
      </div>
      <div className={styles.actions}>
        <Button
          disabled={disabledButton || loading}
          loading={loading}
          title={"Сохранить"}
          onClick={save}
        />
        <div className={styles.response}>
          <Label label={"Response"} />
          <div
            id={"response"}
            className={styles.responseText}
            ref={responseRef}
          />
        </div>
      </div>
    </div>
  );
};

export default Form;
