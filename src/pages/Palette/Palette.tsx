//core
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
//redux
import { paletteActions } from "@redux/actions/palette";
//components
import { ButtonFake } from "@components/Button";
import { InputColor } from "@components/Input";
import { Square } from "@components/Scuare";
//styles
import styles from "@pages/Palette/Palette.module.scss";

const Palette = () => {
  const dispatch = useDispatch();
  const { colors } = useSelector((store: any) => store.palette);
  const [newColor, setNewColor] = useState<string | null>(null);

  useEffect(() => {
    window.addEventListener("blur", () => {
      setNewColor(null);
    });
    return () => {
      window.removeEventListener("blur", () => {
        setNewColor(null);
      });
    };
  }, []);

  const add = useCallback(() => {
    newColor && dispatch(paletteActions.addColor(newColor));
    setNewColor(null);
  }, [newColor]);

  const remove = (index: number) => {
    dispatch(paletteActions.removeColor(index));
  };

  const update = (index: number, color: string) => {
    dispatch(paletteActions.updateColor(index, color));
  };

  return (
    <div className={styles.root}>
      <div className={styles.content}>
        <div className={styles.squares}>
          {newColor && (
            <Square
              id={"new"}
              color={newColor}
              remove={() => setNewColor(null)}
            />
          )}
          {colors.map((color: string, index: number) => {
            return (
              <Square
                key={index}
                id={index}
                color={color}
                remove={() => remove(index)}
                update={(color: string) => update(index, color)}
              />
            );
          })}
        </div>
      </div>
      <div className={styles.actions}>
        <InputColor onChange={(color: string) => setNewColor(color)}>
          <ButtonFake title={"Выбрать цвет"} />
        </InputColor>
      </div>
      {newColor && <div className={styles.backdrop} onClick={add} />}
    </div>
  );
};

export default Palette;
