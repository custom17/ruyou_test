import { paletteTypes } from '@redux/types/palette'

export const paletteActions = {
	addColor: (color: string): any => {
		return (dispatch: any, getStat: any) => {
			const colors = getStat().palette.colors

			dispatch({
				type: paletteTypes.ADD_COLOR,
				payload: [color, ...colors],
			})
		}
	},

	removeColor: (index: number): any => {
		return (dispatch: any, getStat: any) => {
			const colors = getStat().palette.colors

			colors.splice(index, 1)

			dispatch({
				type: paletteTypes.REMOVE_COLOR,
				payload: [...colors],
			})
		}
	},

	updateColor: (index: number, color: string): any => {
		return (dispatch: any, getStat: any) => {
			const colors = getStat().palette.colors

			colors.splice(index, 1, color)

			dispatch({
				type: paletteTypes.UPDATE_COLOR,
				payload: [...colors],
			})
		}
	},
}
