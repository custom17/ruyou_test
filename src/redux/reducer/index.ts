import { combineReducers } from 'redux'

import { paletteReducer } from '@redux/reducer/palette'

export const reducer = combineReducers({
	palette: paletteReducer,
})
