import { paletteTypes } from "@redux/types/palette";

export interface StoreProps {
  colors: string[];
}

export const paletteReducer = (
  store: StoreProps = {
    colors: [],
  },
  action: { type: string; payload: string[] }
) => {
  switch (action.type) {
    case paletteTypes.ADD_COLOR:
      return { ...store, colors: action.payload };
    case paletteTypes.REMOVE_COLOR:
      return { ...store, colors: action.payload };
    case paletteTypes.UPDATE_COLOR:
      return { ...store, colors: action.payload };
    default:
      return store;
  }
};
