import {
  compose,
  legacy_createStore as createStore,
  applyMiddleware,
  Store,
} from "redux";
import thunk from "redux-thunk";

import { reducer } from "@redux/reducer";

const middleware = compose(applyMiddleware(thunk));

const store: Store = createStore(reducer, middleware);

export default store;
