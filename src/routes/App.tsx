//core
import React from "react";
import { Routes, Navigate, Route } from "react-router-dom";
//data
import { book } from "@routes/book";
//components
import { Layout } from "@layouts/Layout";
//interfaces
import { IPage } from "@interfaces/IPage";

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        {book.map((page: IPage) => {
          return <Route key={page.id} path={page.to} element={page.element} />;
        })}
        <Route path="*" element={<Navigate to="/" />} />
      </Route>
    </Routes>
  );
};

export default App;
