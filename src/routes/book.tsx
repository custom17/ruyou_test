//core
import React, { ReactElement } from "react";
//components
import { Form } from "@pages/Form";
import { Palette } from "@pages/Palette";
//interfaces
import { IPage } from "@interfaces/IPage";

export const book: IPage[] = [
  {
    id: 1,
    to: "/",
    title: "Форма",
    element: <Form />,
  },
  {
    id: 2,
    to: "/palette",
    title: "Палитра",
    element: <Palette />,
  },
];
